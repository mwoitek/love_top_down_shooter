local draw = love.graphics.draw
local get_width = love.graphics.getWidth
local get_height = love.graphics.getHeight

local function get_sprite()
  local sprite = {}
  sprite.image = love.graphics.newImage "sprites/bullet.png"
  sprite.scale = { x = 0.5, y = 0.5 }
  sprite.offset = {
    x = sprite.image:getWidth() / 2,
    y = sprite.image:getHeight() / 2,
  }
  return sprite
end

local Bullet = {
  x = 0,
  y = 0,
  speed = 500,
  dead = false,
  sprite = get_sprite(),
  direction = 0,
  vel_x = 0,
  vel_y = 0,
}
Bullet.__index = Bullet

function Bullet:new(obj)
  obj = obj or {}
  setmetatable(obj, self)
  return obj
end

function Bullet:spawn(props)
  local bullet = self:new(props)
  bullet.vel_x = math.cos(bullet.direction) * bullet.speed
  bullet.vel_y = math.sin(bullet.direction) * bullet.speed
  return bullet
end

function Bullet:draw()
  draw(
    self.sprite.image,
    self.x,
    self.y,
    nil,
    self.sprite.scale.x,
    self.sprite.scale.y,
    self.sprite.offset.x,
    self.sprite.offset.y
  )
end

function Bullet:update_position(dt)
  self.x = self.x + self.vel_x * dt
  self.y = self.y + self.vel_y * dt
end

function Bullet:is_offscreen()
  return self.x < 0 or self.y < 0 or self.x > get_width() or self.y > get_height()
end

local Bullets = { arr = {} }
Bullets.__index = Bullets

function Bullets:new()
  local obj = { arr = {} }
  setmetatable(obj, self)
  return obj
end

function Bullets:spawn_bullet(props)
  local bullet = Bullet:spawn(props)
  self.arr[#self.arr + 1] = bullet
end

function Bullets:draw()
  for _, bullet in ipairs(self.arr) do
    bullet:draw()
  end
end

function Bullets:update_position(dt)
  for _, bullet in ipairs(self.arr) do
    bullet:update_position(dt)
  end
end

function Bullets:remove_all()
  for i, _ in ipairs(self.arr) do
    self.arr[i] = nil
  end
end

function Bullets:remove_dead()
  for i = #self.arr, 1, -1 do
    if self.arr[i].dead then table.remove(self.arr, i) end
  end
end

function Bullets:remove_offscreen()
  for i = #self.arr, 1, -1 do
    if self.arr[i]:is_offscreen() then table.remove(self.arr, i) end
  end
end

return Bullets
