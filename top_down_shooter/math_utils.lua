local M = {}

function M.angle(x1, y1, x2, y2)
  return math.atan2(y2 - y1, x2 - x1)
end

function M.distance(x1, y1, x2, y2)
  return math.sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2)
end

function M.normalize_vector(vector)
  local norm = math.sqrt(vector[1] ^ 2 + vector[2] ^ 2)
  vector[1] = vector[1] / norm
  vector[2] = vector[2] / norm
  return vector
end

return M
