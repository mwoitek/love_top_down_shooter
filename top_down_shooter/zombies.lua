local math_utils = require "math_utils"
local draw = love.graphics.draw

local function get_sprite()
  local sprite = {}
  sprite.image = love.graphics.newImage "sprites/zombie.png"
  sprite.offset = {
    x = sprite.image:getWidth() / 2,
    y = sprite.image:getHeight() / 2,
  }
  return sprite
end

local Zombie = {
  x = 0,
  y = 0,
  speed = 140,
  dead = false,
  sprite = get_sprite(),
}
Zombie.__index = Zombie

function Zombie:new(obj)
  obj = obj or {}
  setmetatable(obj, self)
  return obj
end

function Zombie:set_random_position()
  local side = math.random(1, 4)
  if side == 1 then -- left
    self.x = -self.sprite.image:getWidth()
    self.y = math.random(0, love.graphics.getHeight())
  elseif side == 2 then -- right
    self.x = self.sprite.image:getWidth() + love.graphics.getWidth()
    self.y = math.random(0, love.graphics.getHeight())
  elseif side == 3 then -- top
    self.x = math.random(0, love.graphics.getWidth())
    self.y = -self.sprite.image:getHeight()
  else -- bottom
    self.x = math.random(0, love.graphics.getWidth())
    self.y = self.sprite.image:getHeight() + love.graphics.getHeight()
  end
end

function Zombie:spawn(speed)
  local props = speed and { speed = speed } or nil
  local zombie = self:new(props)
  zombie:set_random_position()
  return zombie
end

function Zombie:get_angle(x, y)
  return math_utils.angle(self.x, self.y, x, y)
end

function Zombie:draw(player_x, player_y)
  draw(
    self.sprite.image,
    self.x,
    self.y,
    self:get_angle(player_x, player_y),
    nil,
    nil,
    self.sprite.offset.x,
    self.sprite.offset.y
  )
end

function Zombie:is_close(x, y, dist)
  return math_utils.distance(self.x, self.y, x, y) < dist
end

local Zombies = { arr = {} }
Zombies.__index = Zombies

function Zombies:new()
  local obj = { arr = {} }
  setmetatable(obj, self)
  return obj
end

function Zombies:spawn_zombie(speed)
  local zombie = Zombie:spawn(speed)
  self.arr[#self.arr + 1] = zombie
end

function Zombies:draw(player_x, player_y)
  for _, zombie in ipairs(self.arr) do
    zombie:draw(player_x, player_y)
  end
end

function Zombies:remove_all()
  for i, _ in ipairs(self.arr) do
    self.arr[i] = nil
  end
end

function Zombies:remove_dead()
  for i = #self.arr, 1, -1 do
    if self.arr[i].dead then table.remove(self.arr, i) end
  end
end

return Zombies
