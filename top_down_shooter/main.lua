local MAX_TIME = 2
local TIME_REDUCTION_FACTOR = 0.98
local DIST_PLAYER = 30
local DIST_BULLET = 20

local is_playing = false
local score = 0
local max_time = MAX_TIME
local timer = MAX_TIME

local player
local zombies
local bullets
local background

local draw
local get_width
local get_height
local printf

math.randomseed(os.time())
local cos = math.cos
local sin = math.sin

function love.load()
  player = require("player"):initialize()
  zombies = require("zombies"):new()
  bullets = require("bullets"):new()
  background = love.graphics.newImage "sprites/background.png"

  draw = love.graphics.draw
  get_width = love.graphics.getWidth
  get_height = love.graphics.getHeight
  printf = love.graphics.printf

  local my_font = love.graphics.newFont(30)
  love.graphics.setFont(my_font)
end

function love.update(dt)
  if not is_playing then return end

  player:update_position(dt)

  for _, zombie in ipairs(zombies.arr) do
    local angle = zombie:get_angle(player.x, player.y)
    local ds = zombie.speed * dt
    zombie.x = zombie.x + cos(angle) * ds
    zombie.y = zombie.y + sin(angle) * ds

    if zombie:is_close(player.x, player.y, DIST_PLAYER) then
      player.dead = true
      break
    end
  end

  if player.dead then
    is_playing = false
    player:reset()
    zombies:remove_all()
    bullets:remove_all()
  end

  bullets:update_position(dt)
  bullets:remove_offscreen()

  for _, zombie in ipairs(zombies.arr) do
    for _, bullet in ipairs(bullets.arr) do
      if zombie:is_close(bullet.x, bullet.y, DIST_BULLET) then
        score = score + 1
        zombie.dead = true
        bullet.dead = true
      end
    end
  end

  zombies:remove_dead()
  bullets:remove_dead()

  timer = timer - dt
  if timer <= 0 then
    zombies:spawn_zombie()
    max_time = max_time * TIME_REDUCTION_FACTOR
    timer = max_time
  end
end

function love.draw()
  draw(background, 0, 0)
  printf("Score: " .. tostring(score), 0, get_height() - 100, get_width(), "center")
  player:draw(is_playing)

  if is_playing then
    zombies:draw(player.x, player.y)
    bullets:draw()
  else
    printf("Click anywhere to begin!", 0, 50, get_width(), "center")
  end
end

function love.mousepressed(_, _, button)
  if button ~= 1 then return end

  if is_playing then
    bullets:spawn_bullet {
      x = player.x,
      y = player.y,
      direction = player:get_mouse_angle(),
    }
  else
    is_playing = true
    score = 0
    max_time = MAX_TIME
    timer = MAX_TIME
  end
end
