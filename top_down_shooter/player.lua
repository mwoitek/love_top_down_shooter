local math_utils = require "math_utils"

local draw = love.graphics.draw
local get_width = love.graphics.getWidth
local get_height = love.graphics.getHeight
local is_key_down = love.keyboard.isDown
local mouse_get_x = love.mouse.getX
local mouse_get_y = love.mouse.getY

local function get_sprite()
  local sprite = {}
  sprite.image = love.graphics.newImage "sprites/player.png"
  sprite.offset = {
    x = sprite.image:getWidth() / 2,
    y = sprite.image:getHeight() / 2,
  }
  return sprite
end

local Player = {
  x = 0,
  y = 0,
  speed = 180,
  dead = false,
  sprite = get_sprite(),
}

Player.min_x = Player.sprite.offset.x
Player.max_x = get_width() - Player.sprite.offset.x

Player.min_y = Player.sprite.offset.y
Player.max_y = get_height() - Player.sprite.offset.y

Player.__index = Player

function Player:new(obj)
  obj = obj or {}
  setmetatable(obj, self)
  return obj
end

function Player:move_to_center()
  self.x = get_width() / 2
  self.y = get_height() / 2
end

function Player:initialize(speed)
  local props = speed and { speed = speed } or nil
  local player = self:new(props)
  player:move_to_center()
  return player
end

function Player:get_mouse_angle()
  return math_utils.angle(self.x, self.y, mouse_get_x(), mouse_get_y())
end

function Player:draw(is_playing)
  draw(
    self.sprite.image,
    self.x,
    self.y,
    is_playing and self:get_mouse_angle() or math.pi / 2,
    nil,
    nil,
    self.sprite.offset.x,
    self.sprite.offset.y
  )
end

function Player:update_position(dt)
  local vec = { 0, 0 }

  if is_key_down("d", "right") then vec[1] = vec[1] + 1 end
  if is_key_down("a", "left") then vec[1] = vec[1] - 1 end
  if is_key_down("w", "up") then vec[2] = vec[2] - 1 end
  if is_key_down("s", "down") then vec[2] = vec[2] + 1 end

  if vec[1] == 0 and vec[2] == 0 then return end
  if vec[1] ~= 0 and vec[2] ~= 0 then vec = math_utils.normalize_vector(vec) end

  local ds = self.speed * dt
  self.x = self.x + vec[1] * ds
  self.y = self.y + vec[2] * ds

  if self.x < self.min_x then
    self.x = self.min_x
  elseif self.x > self.max_x then
    self.x = self.max_x
  end

  if self.y < self.min_y then
    self.y = self.min_y
  elseif self.y > self.max_y then
    self.y = self.max_y
  end
end

function Player:reset()
  self.dead = false
  self:move_to_center()
end

return Player
